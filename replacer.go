package places

import (
	"bytes"
	"io"
)

// ReplaceStringFunc is a func implementing Replacer
type ReplaceStringFunc func(string) string

func (m ReplaceStringFunc) Replace(wr io.Writer, key string) {
	res := m(key)
	if len(res) > 0 {
		wr.Write([]byte(res))
	}
}

var _ Replacer = ReplaceStringFunc(func(string) string {
	return ""
})

// ReplaceFunc is a func implementing  Replacer
type ReplaceFunc func(wr io.Writer, key string)

func (m ReplaceFunc) Replace(wr io.Writer, key string) {
	m(wr, key)
}

var _ Replacer = ReplaceFunc(func(wr io.Writer, key string) {})

// WriterToMap is a writer map that implements Replacer
type WriterToMap map[string]io.WriterTo

func (r WriterToMap) Replace(wr io.Writer, key string) {
	// lookup the position name within the replacements and
	// write the replacement if we found one
	wrt := r[key]
	if wrt != nil {
		wrt.WriteTo(wr)
	}
}

var _ Replacer = WriterToMap{}

// ReadSeekerMap is a readseeker map that implements Replacer
type ReadSeekerMap map[string]io.ReadSeeker

func (r ReadSeekerMap) Replace(wr io.Writer, key string) {
	// lookup the position name within the replacements and
	// write the replacement if we found one
	rs := r[key]
	if rs != nil {
		rs.Seek(0, 0)
		io.Copy(wr, rs)
	}
}

var _ Replacer = ReadSeekerMap{}

// StringMap is a string map that implements Replacer
type StringMap map[string]string

func (m StringMap) Replace(wr io.Writer, key string) {
	v := m[key]
	if len(v) > 0 {
		wr.Write([]byte(v))
	}
}

var _ Replacer = StringMap{}

// BytesMap is a string map that implements Replacer
type BytesMap map[string][]byte

func (m BytesMap) Replace(wr io.Writer, key string) {
	bt := m[key]
	if len(bt) > 0 {
		wr.Write(bt)
	}
}

var _ Replacer = BytesMap{}

// ReplacerMap is a string map that implements Replacer
type ReplacerMap map[string]Replacer

func (m ReplacerMap) Replace(wr io.Writer, key string) {
	repl := m[key]
	if repl != nil {
		repl.Replace(wr, key)
	}
}

var _ Replacer = ReplacerMap{}

// FromWriterTo returns a Replacer that is getting the data from the Writerto
func FromWriterTo(wrt io.WriterTo, errHandler func(error)) Replacer {
	return ReplaceStringFunc(func(position string) string {
		var bf bytes.Buffer
		_, err := wrt.WriteTo(&bf)
		if err != nil {
			callErrHandler(err, errHandler)
			return ""
		}

		return bf.String()
	})
}

// FromReadSeeker returns a Replacer that is getting the data from the ReadSeeker
func FromReadSeeker(rs io.ReadSeeker, errHandler func(error)) Replacer {
	return ReplaceStringFunc(func(position string) string {
		_, err := rs.Seek(0, 0)

		if err != nil {
			callErrHandler(err, errHandler)
			return ""
		}

		var b []byte
		b, err = io.ReadAll(rs)

		if err != nil {
			callErrHandler(err, errHandler)
			return ""
		}

		return string(b)
	})
}

// FromReader returns a Replacer that is getting the data from the Reader
func FromReader(rd io.Reader, errHandler func(error)) Replacer {
	return ReplaceStringFunc(func(position string) string {
		bt, err := io.ReadAll(rd)
		if err != nil {
			callErrHandler(err, errHandler)
			return ""
		}

		return string(bt)
	})
}

func Echo(s string) string {
	return s
}

// Empty is a Replacer that always returns an empty string
type Empty struct{}

func (e Empty) Replace(wr io.Writer, key string) {
	return
}

var _ Replacer = Empty{}
var _ Replacer = String("")
var _ Replacer = Self("")

// String is a Relacer that always returns the string itself
type String string

func (s String) Replace(wr io.Writer, position string) {
	wr.Write([]byte(s))
}

type Self string

// Self is a Replacer that always returns the position prefixed by the value of Self
func (s Self) Replace(wr io.Writer, position string) {
	wr.Write([]byte(string(s) + position))
}

func callErrHandler(err error, errHandler func(error)) {
	if errHandler != nil {
		errHandler(err)
	}
}
