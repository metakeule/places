package wrap

import (
	"bytes"
	"html"
	"io"
	"net/url"
	"regexp"
	"sync"

	"gitlab.com/metakeule/places"
)

/*

The idea is to be able to wrap and delegate the replacement to different replacers, have prefixes in order to do escaping etc.

*/

var (
	EscapeHTML = places.ReplaceStringFunc(html.EscapeString)
	EscapeUrl  = places.ReplaceStringFunc(url.QueryEscape)
	EscapeNot  = places.ReplaceStringFunc(places.Echo)
)

type Option func(*wrap)

func TrimPrefix() Option {
	return func(m *wrap) {
		m.trimPrefix = true
	}
}

var prefixRule = regexp.MustCompile("^[a-z]+$")

type wrap struct {
	m          map[string]places.Replacer
	trimPrefix bool
}

// Wrap wraps the inner mapper with the outper mapper
func (mp *wrap) Wrap(innerMapper places.Replacer) places.Replacer {
	return places.ReplaceFunc(func(wr io.Writer, position string) {
		prefix, rest := SplitPrefix(position)
		key := position

		var bf bytes.Buffer

		if mp.trimPrefix && rest != "" {
			key = prefix
			innerMapper.Replace(&bf, rest)
		} else {
			innerMapper.Replace(&bf, position)
		}

		res := bf.String()

		// If a prefix is identified but does not conform to prefixRule,
		// an empty string will be returned, since Add makes sure the every prefix
		// in the map conforms
		m, ok := mp.m[key]

		if !ok || m == nil {
			return
		}

		m.Replace(wr, res)
	})
}

func (mp *wrap) Replace(wr io.Writer, position string) {

	if !mp.trimPrefix {
		ma := mp.m[position]
		if ma == nil {
			return
		}
		ma.Replace(wr, position)
		return
	}

	prefix, rest := SplitPrefix(position)

	// If a prefix is identified but does not conform to prefixRule,
	// an empty string will be returned, since Add makes sure the every prefix
	// in the map conforms
	m, ok := mp.m[prefix]

	if !ok || m == nil {
		return
	}

	if rest != "" {
		m.Replace(wr, rest)
		return
	}

	m.Replace(wr, position)
}

// Add registers a replacer in the registry for the given key
// If the TrimPrefix option was given, the key will be treated as prefix
// If there is already a mapper for the given prefix, it will be overwritten
// If prefix is the empty string, the default mapper is set.
// If prefix does not conform to the regular expression ^[a-z]+$, ErrInvalidPrefix is returned
// If a mapper already exists for this prefix, MapperAlreadyExistsError is returned
func (mp *wrap) Add(key string, repl places.Replacer) error {

	if !mp.trimPrefix {
		mp.m[key] = repl
		return nil
	}

	if key != "" && !prefixRule.MatchString(key) {
		return ErrInvalidPrefix
	}
	if _, has := mp.m[key]; has {
		return ErrMapperAlreadyExists(key)
	}
	mp.m[key] = repl
	return nil
}

// New returns a new Map that is not safe for concurrent use.
func New(opts ...Option) Wrapper {
	mp := &wrap{
		m:          map[string]places.Replacer{},
		trimPrefix: false,
	}

	for _, opt := range opts {
		opt(mp)
	}

	return mp
}

// NewConcurrent returns a new Map that is safe for concurrent use.
func NewConcurrent(opts ...Option) Wrapper {
	return &wrap_concurrent{m: New(opts...)}
}

type wrap_concurrent struct {
	mx sync.RWMutex
	m  Wrapper
}

func (c *wrap_concurrent) Wrap(valueMapper places.Replacer) places.Replacer {
	c.mx.RLock()
	res := c.m.Wrap(valueMapper)
	c.mx.RUnlock()
	return res
}

func (c *wrap_concurrent) Replace(wr io.Writer, position string) {
	c.mx.RLock()
	c.m.Replace(wr, position)
	c.mx.RUnlock()
}

func (c *wrap_concurrent) Add(prefix string, mapper places.Replacer) error {
	c.mx.Lock()
	err := c.m.Add(prefix, mapper)
	c.mx.Unlock()
	return err
}
