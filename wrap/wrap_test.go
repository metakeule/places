package wrap

import (
	"bytes"
	"strings"
	"testing"

	"gitlab.com/metakeule/places"
)

func TestEscapingMap(t *testing.T) {

	var bf bytes.Buffer

	tt := `
<html>
<body>
<p class="(@-css class@)">Hi, (@-html -db user_name@)</p>
<a href="https://google.co?s=(@-url link@)">Click me</a>
<code>
(@-html -string code@)
</code>
</body>
</html>
`

	expected := `
<html>
<body>
<p class="shiny">Hi, &lt;d&gt;onald</p>
<a href="https://google.co?s=www.xyz.com">Click me</a>
<code>
&lt;i&gt;italic&lt;/i&gt;
</code>
</body>
</html>
`

	sMap := New()
	sMap.Add("class", places.String("shiny"))
	sMap.Add("link", places.String("www.xyz.com"))
	sMap.Add("code", places.String(`<i>italic</i>`))

	dbMap := New()
	dbMap.Add("user_name", places.FromReader(strings.NewReader("<d>onald"), nil))

	valueMapper := New(TrimPrefix())
	valueMapper.Add("db", dbMap)
	valueMapper.Add("string", sMap)
	valueMapper.Add("", sMap)

	escapingMapper := New(TrimPrefix())
	escapingMapper.Add("css", EscapeNot)
	escapingMapper.Add("html", EscapeHTML)
	escapingMapper.Add("url", EscapeUrl)
	final := escapingMapper.Wrap(valueMapper)

	places.FindAndReplace([]byte(tt), &bf, final, places.WithDelimiter("(@", "@)"))

	got := bf.String()

	if got != expected {
		t.Errorf("got:\n%s\n expected\n%s\n", got, expected)
	}
}
