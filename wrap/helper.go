package wrap

import "strings"

// SplitPrefix splits the given input and returns the prefix and rest.
// If the first character is a minus sign ("-") then everything that follows
// up to the next whitespace ("\s") is considered to be the prefix and everything
// following that whitespace is considered to be the rest
// If the first character is no minus sign, the prefix is empty and the rest equals the
// input.
// If input is the empty string or just the minus sign, prefix and rest are empty strings.
func SplitPrefix(input string) (prefix string, rest string) {
	prefix, rest = _split(input)
	//	fmt.Printf("found prefix %q and rest %q for position %q\n", prefix, rest, input)
	return
}

func _split(input string) (prefix string, rest string) {
	if input == "" || input == "-" {
		return
	}

	if input[0] != '-' {
		rest = input
		return
	}

	idx := strings.IndexRune(input, ' ')

	if idx == -1 {
		prefix = input[1:]
		return
	}

	prefix = input[1:idx]
	rest = strings.TrimSpace(input[idx:])
	return
}

func callErrHandler(err error, errHandler func(error)) {
	if errHandler != nil {
		errHandler(err)
	}
}
