package wrap

import (
	"gitlab.com/metakeule/places"
)

// Wrapper is a registry of places.Mappers and can wrap a mapper by escaping values
type Wrapper interface {
	places.Replacer

	// Add registers a mapper in the registry for the given key
	Add(key string, m places.Replacer) error

	// Wrap wraps the outer mapping around the inner replacer
	Wrap(inner places.Replacer) places.Replacer
}
