package wrap

import (
	"errors"
	"fmt"
)

var ErrInvalidPrefix = errors.New("prefix does not match the regular expression ^[a-z]+$")

type ErrMapperAlreadyExists string

func (m ErrMapperAlreadyExists) Error() string {
	return fmt.Sprintf("Mapper for prefix %#v already exists", m)
}
