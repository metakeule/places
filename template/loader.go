package template

import (
	"bytes"
	"io"
	"os"
	"path/filepath"
	"regexp"
)

func NewLoader(rootDir string, extension string, ignoreDirs *regexp.Regexp) *Loader {
	return &Loader{
		rootDir:    rootDir,
		extension:  extension,
		ignoreDirs: ignoreDirs,
	}
}

// Loader loads templates recursively from a root directory for a given file extension
type Loader struct {
	*ReadSeeker
	rootDir    string
	extension  string
	ignoreDirs *regexp.Regexp // directories to be ignored
}

func (l *Loader) fileReader(path string) (io.ReadSeeker, error) {
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(b), nil
}

func (l *Loader) walk(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if l.ignoreDirs != nil && info.IsDir() && l.ignoreDirs.MatchString(info.Name()) {
		return filepath.SkipDir
	}

	if !info.IsDir() && filepath.Ext(path) == l.extension {
		var rel string
		rel, err = filepath.Rel(l.rootDir, path)
		if err != nil {
			return err
		}
		var rd io.ReadSeeker
		rd, err = l.fileReader(path)
		if err != nil {
			return err
		}
		l.ReadSeeker.Add(rel, rd)
	}
	return nil
}

func (l *Loader) Load() (*ReadSeeker, error) {
	info, errStat := os.Stat(l.rootDir)

	if errStat != nil {
		if os.IsNotExist(errStat) {
			return nil, ErrRootDoesNotExist(l.rootDir)
		}
		return nil, errStat
	}

	if !info.IsDir() {
		return nil, ErrRootIsNoDir(l.rootDir)
	}

	l.ReadSeeker = NewReadSeeker()

	err := filepath.Walk(l.rootDir, l.walk)

	if err != nil {
		return nil, err
	}

	return l.ReadSeeker, nil
}
