/*
package template provides a places.Mapper to allow embedding of templates
*/

package template

import (
	"bytes"
	"io"

	"gitlab.com/metakeule/places"
)

type NMapper interface {
	places.Replacer
	NMap(n int, sub string) places.Replacer
	Len() int
}

type mapDelegate struct {
	first, second places.Replacer
}

func (m *mapDelegate) Replace(wr io.Writer, input string) {
	var bf bytes.Buffer
	m.first.Replace(&bf, input)
	out := bf.String()
	if out != "" {
		wr.Write([]byte(out))
		return
	}
	m.second.Replace(wr, input)
}
