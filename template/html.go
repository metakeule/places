package template

import (
	"bytes"
	"fmt"
	"html"
	"io"
	"net/url"
	"strings"
	"sync"

	"gitlab.com/metakeule/places"
	"gitlab.com/metakeule/places/wrap"
)

type HTML struct {
	sync.RWMutex
	rs  *ReadSeeker
	rsm map[string]*places.Cache
}

func NewHTML(rs *ReadSeeker) *HTML {
	h := &HTML{
		rs:  rs,
		rsm: map[string]*places.Cache{},
	}
	h.rs.mx.RLock()

	for k, rs := range h.rs.m {
		_, err := rs.Seek(0, 0)
		if err == nil {
			var b []byte
			b, err = io.ReadAll(rs)
			if err == nil {
				h.rsm[k] = places.New(b)
			}
		}
	}
	h.rs.mx.RUnlock()
	return h
}

func (h *HTML) NewMapper(m map[string]places.Replacer) *HTMLReplacer {
	return &HTMLReplacer{HTML: h, m: m}
}

type HTMLReplacer struct {
	sync.Mutex
	*HTML
	m         map[string]places.Replacer
	preferred places.Replacer
	indexes   []NMapper // keep track of array indexes within nested objects
	depth     int       // current depth of nested objects
}

func (h *HTMLReplacer) require(name string, m places.Replacer) string {
	// fmt.Printf("requiring: %#v\n", name)
	h.HTML.RLock()
	defer h.HTML.RUnlock()

	if t, ok := h.HTML.rsm[name]; ok {
		var bf bytes.Buffer
		t.Replace(&bf, m)
		return bf.String()
	}
	return ""
}

/*
func (h *HTMLTemplateMapper) _map_delegate(input string, m places.Mapper) string {
	out := m.Map(input)
	if out != "" {
		return out
	}
	return h._map(input)
}
*/

func (h *HTMLReplacer) Replace(wr io.Writer, input string) {
	if h.preferred != nil {
		h.preferred.Replace(wr, input)
		return
	}
	h._map(input)
}

func (h *HTMLReplacer) findMapper(depth int) NMapper {
	h.Lock()
	defer h.Unlock()
	if len(h.indexes) > depth {
		return nil
	}
	return h.indexes[depth-1]
}

// findNestedMapper finds a mapper for a nested object
func (h *HTMLReplacer) findNestedMapper(sub string) places.Replacer {
	//fmt.Printf("inside findNestedMapper: %#v, depth: %d\n", sub, h.depth)
	if sub == "" {
		return places.String("")
	}
	sb := strings.Split(sub, ".")

	if len(sb) != h.depth {
		return places.String("[Error] too deep var declaration")
	}

	var m places.Replacer

	for d := 0; d <= len(sb); d++ {
		nm := h.findMapper(d)

		if nm == nil {
			return m
		}

		m = nm
	}

	return m
}

func (h *HTMLReplacer) replaceVars(bf places.Buffer, t *places.Cache, nm NMapper, sub string) {
	//fmt.Printf("replaceVars for mapper %#v, sub: %#v\n", nm, sub)
	/*
		if sub != "" {
			for i := 0; i < l; i++ {
				var m = nm.NMap(i, sub)
				if nmm, isNM := m.(NMapper); isNM {
					h.replaceVars(bf, t, nmm, sub)
					continue
				}
				h.preferred = nm
				t.ReplaceMapper(bf, h)
				h.preferred = nil
			}
			return
		}
	*/
	/*
		idx := strings.IndexRune(sub, '.')
		if idx == -1 {
			sub = ""
		} else {
			sub = sub[idx+1:]
		}
	*/

	// h.depth++

	l := nm.Len()
	for i := 0; i < l; i++ {
		var m = nm.NMap(i, sub)
		if nmm, isNM := m.(NMapper); isNM {
			h.replaceVars(bf, t, nmm, sub)
		} else {
			fmt.Printf("got mapper: %#v[%d]\n", m, i)
			h.preferred = m
			t.Replace(bf, h)
			h.preferred = nil
		}
	}
	/*
		// h.depth--
		fmt.Printf("indexes: %#v, depth: %d, sub: %#v\n", h.indexes, h.depth, sub)
		if h.depth == 1 && sub != "" {
			fmt.Printf("now calling findNestedMapper\n")
			h.depth = len(strings.Split(sub, "."))
			h.preferred = h.findNestedMapper(sub)
			fmt.Printf("found nested mapper: %#v\n", h.preferred)
			t.ReplaceMapper(bf, h)
			h.indexes = []NMapper{}
			h.depth = 0
		}
		h.preferred = nil
	*/
}

func (h *HTMLReplacer) _map(input string) string {
	prefix, rest := wrap.SplitPrefix(input)

	//fmt.Printf("prefix: %#v rest: %#v\n", prefix, rest)
	if prefix == "require" {
		return h.require(rest, h)
	}

	if prefix == "each" {
		s := strings.SplitN(rest, " ", 2)
		mpName, inc := strings.TrimSpace(s[0]), strings.TrimSpace(s[1])
		var sub string

		if strings.ContainsRune(mpName, '.') {
			sp := strings.SplitN(mpName, ".", 2)
			mpName = sp[0]
			sub = sp[1]
		}

		//fmt.Printf("mpName: %#v, inc: %#v\n", mpName, inc)
		h.Lock()
		mp, ok := h.m[mpName]
		h.Unlock()
		if !ok {
			//fmt.Printf("mpName %#v not found", mpName)
			return ""
		}

		h.HTML.RLock()
		t, hasTemplate := h.HTML.rsm[inc]
		h.HTML.RLock()
		if !hasTemplate {
			//fmt.Printf("template %#v not found", inc)
			return ""
		}

		// TODO: debug this properly
		if nm, is := mp.(NMapper); is {
			h.depth++
			h.indexes = append(h.indexes, NMapper(nil))
			var bf bytes.Buffer
			l := nm.Len()
			for i := 0; i < l; i++ {
				var m = nm.NMap(i, sub)
				//fmt.Printf("got mapper: %#v[%d]\n", m, i)
				if nmm, isNM := m.(NMapper); isNM {
					h.indexes[h.depth-1] = nmm
					h.replaceVars(&bf, t, nmm, sub)
				} else {
					// h.depth--
					// fmt.Printf("indexes: %#v, depth: %d, sub: %#v\n", h.indexes, h.depth, sub)
					if sub != "" {
						//	fmt.Printf("now calling findNestedMapper\n")
						h.depth = len(strings.Split(sub, "."))
						h.preferred = h.findNestedMapper(sub)
						//fmt.Printf("found nested mapper: %#v\n", h.preferred)
						t.Replace(&bf, h)
						h.indexes = []NMapper{}
						h.depth = 0
					} else {

						h.preferred = nil
						h.preferred = m
						t.Replace(&bf, h)
						h.preferred = nil
					}
				}
			}

			h.replaceVars(&bf, t, nm, sub)
			h.preferred = nil
			h.indexes = h.indexes[:len(h.indexes)-1]
			h.depth--
			return bf.String()
		} else {
			return fmt.Sprintf("not a NMapper: %#v\n", mp)
		}
	}

	h.Lock()
	mp, ok := h.m[rest]
	h.Unlock()
	if !ok {
		return ""
	}

	var bbf bytes.Buffer

	switch prefix {
	case "js":
		mp.Replace(&bbf, rest)
		return fmt.Sprintf("%#v", bbf.String())
	case "raw":
		mp.Replace(&bbf, rest)
		return bbf.String()
	case "html":
		mp.Replace(&bbf, rest)
		return bbf.String()
	case "url":
		mp.Replace(&bbf, rest)
		return url.QueryEscape(bbf.String())
	case "include":
		mp.Replace(&bbf, strings.TrimSpace(rest))
		if val := bbf.String(); val != "" {
			return h.require(val, h)
		}
		return ""
	default:
		mp.Replace(&bbf, rest)
		return html.EscapeString(bbf.String())
	}

}

/*
func NewHTML(rootDir string, ignoreDirs *regexp.Regexp, m map[string]string) (places.Mapper, error) {
	l := NewLoader(rootDir, ".html", ignoreDirs)
	rs, err := l.Load()
	if err != nil {
		return nil, err
	}

	t := NewHTMLTemplate(rs, m)

	return t, nil
}
*/
