package template

import (
	"io"
	"sync"
)

// TODO: we want to get sid of this, since we can just use a normal placesmap.New() and then wrap the values with a placesmap.ReadSeekerFunc

// ReadSeeker is a map of strings to io.ReadSeeker that may be used concurrently
type ReadSeeker struct {
	mx sync.RWMutex
	m  map[string]io.ReadSeeker
}

// NewReadSeeker returns a
func NewReadSeeker() *ReadSeeker {
	return &ReadSeeker{
		m: map[string]io.ReadSeeker{},
	}
}

// Add adds an io.ReadSeeker for the given name.
// If there is already a ReadSeeker defined for the given name,
// an error is returned.
// There are no restrictions for the name
func (r *ReadSeeker) Add(name string, rs io.ReadSeeker) error {
	if _, has := r.m[name]; has {
		return ErrReadSeekerAlreadyExists(name)
	}
	r.m[name] = rs
	return nil
}

func (r *ReadSeeker) Map(name string) (val string) {
	r.mx.RLock()
	if rs, ok := r.m[name]; ok {
		_, err := rs.Seek(0, 0)
		if err == nil {
			var b []byte
			b, err = io.ReadAll(rs)
			if err == nil {
				val = string(b)
			}
		}
	}

	r.mx.RUnlock()
	return
}
