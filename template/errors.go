package template

import (
	"fmt"
)

type ErrRootIsNoDir string

func (r ErrRootIsNoDir) Error() string {
	return fmt.Sprintf("root %#v is not a directory", r)
}

type ErrRootDoesNotExist string

func (r ErrRootDoesNotExist) Error() string {
	return fmt.Sprintf("root %#v does not exist", r)
}

type ErrReadSeekerAlreadyExists string

func (m ErrReadSeekerAlreadyExists) Error() string {
	return fmt.Sprintf("ReadSeeker for name %#v already exists", m)
}
