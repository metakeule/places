package places

import (
	"bytes"
	"io"
)

/*

Places is a fast and lightweight way of text replacement via positions.
The templates are searched for the places where the positions reside and
this places are tracked and be be saved for a fast reuse.

Then the replacement happens via a Replacer that do the mapping between the
position keys and the values.

Since Replacer is an interface, these values may come from different sources.

*/

// Replacer replaces the position with its value, by writing the value to the given io.Writer
// This is a core component that allows different sources for the replacement.
// Some implementations can be found in the replacer.go file.
// If you want to track errors around io then implement your own Replacer to track them.
type Replacer interface {
	Replace(wr io.Writer, position string)
}

// The Buffer interface is fullfilled by *bytes.Buffer. However since for performance reasons
// the errors from writing to the buffer are always ignored, you will need to write a buffer wrapper to capture them.
type Buffer interface {
	io.Writer
	WriteString(string) (int, error)
}

// Find looks for positions written in the style "[@positionname@]" inside the given template.
// It returns a slice containing the positions of the positions that is meant to be passed to
// Replace or ReplaceString.
func Find(template []byte, opts ...Option) (places []int) {
	conf := defaultOptions()

	for _, opt := range opts {
		opt(conf)
	}

	places = make([]int, 0, 22)

	var (
		found  = -1
		start  int
		end    int
		length = len(template)
	)

	for i := 0; i < length; i++ {

		found = bytes.Index(template[i:], conf.startDel)
		if -1 == found {
			break
		}
		start = found + i

		found = bytes.Index(template[start+2:], conf.endDel)
		if -1 == found {
			break
		}
		end = found + start + 2 // two bytes for each delimiter

		places = append(places, start, end)
		i = end

	}

	return
}

type noopWriter func()

func (noopWriter) Write(bt []byte) (int, error) {
	return len(bt), nil
}

var _ io.Writer = noopWriter(func() {})

// Positions returns all the found Positions in the given template
func Positions(template []byte, places []int) (positions []string) {
	var m = map[string]bool{}
	replfn := ReplaceFunc(func(_ io.Writer, key string) {
		m[key] = true
	})
	noop := noopWriter(func() {})
	Replace(template, noop, places, replfn)

	for k := range m {
		positions = append(positions, k)
	}
	return
}

// Replace replaces the positions at the given places inside the template by
// using the given Replacer.
// The given template must be the unchanged byte array that was passed to Find in order to get the
// places. For strings as replacements see the optimized ReplaceString function for bytes use ReplaceBytes.
func Replace(template []byte, wr io.Writer, places []int, repl Replacer) {
	var (
		last   int
		first  int
		length = len(places)
	)

	// we iterate over places always taking pairs of ints
	// where the first int is the starting and the last is the ending position
	// i.e.
	// "the quick [@colorOfFox@] fox"
	//            |           |
	//           places[i]   places[i+1]
	// so places[i] == 10 and places[i+1] == 22
	// so we can get:
	//   - everything before the place with
	//       template[:places[i]]
	//   - the position name with
	//       template[places[i]+2:places[i+1]]
	//   - everything after the place with
	//       template[places[i+1]+2:]
	//
	// instead of going just from the beginning to the end, we iterate over a cursor (last)
	// from position to position until we are through the template
	for i := 0; i < length; i += 2 {
		// track the first position of the position
		first = places[i]

		// take the bytes from the last position within the template up to the position
		wr.Write(template[last:first])

		// lookup the position name within the replacements and
		// write the replacement if we found one
		repl.Replace(wr, string(template[first+2:places[i+1]]))

		// track the last position for the next iteration
		last = places[i+1] + 2
	}

	wr.Write(template[last:]) // write any remaining parts of the template that don't have any positions
}

// ReplaceBytes is an optimized version of Replace for a bytes map
func ReplaceBytes(template []byte, wr io.Writer, places []int, replacements map[string][]byte) {
	var (
		last   int
		first  int
		length = len(places)
	)

	// we iterate over places always taking pairs of ints
	// where the first int is the starting and the last is the ending position
	// i.e.
	// "the quick [@colorOfFox@] fox"
	//            |           |
	//           places[i]   places[i+1]
	// so places[i] == 10 and places[i+1] == 22
	// so we can get:
	//   - everything before the place with
	//       template[:places[i]]
	//   - the position name with
	//       template[places[i]+2:places[i+1]]
	//   - everything after the place with
	//       template[places[i+1]+2:]
	//
	// instead of going just from the beginning to the end, we iterate over a cursor (last)
	// from position to position until we are through the template
	for i := 0; i < length; i += 2 {
		// track the first position of the position
		first = places[i]

		// take the bytes from the last position within the template up to the position
		wr.Write(template[last:first])

		// lookup the position name within the replacements and
		// write the replacement if we found one
		repl := replacements[string(template[first+2:places[i+1]])]
		if len(repl) > 0 {
			wr.Write(repl)
		}

		// track the last position for the next iteration
		last = places[i+1] + 2
	}

	wr.Write(template[last:]) // write any remaining parts of the template that don't have any positions
}

// FindAndReplace finds positions and replaces them in one go.
func FindAndReplace(template []byte, wr io.Writer, repl Replacer, opts ...Option) {
	Replace(template, wr, Find(template, opts...), repl)
}

// FindAndReplaceString finds positions and replaces them in one go.
func FindAndReplaceString(template string, wr io.Writer, repl Replacer, opts ...Option) {
	FindAndReplace([]byte(template), wr, repl, opts...)
}

// Cache is caching the template and the positions of the positions
type Cache struct {
	places   []int
	template []byte
}

// New parses the given bytes and returns a Cache to remember where the positions are inside the template
func New(t []byte, opts ...Option) *Cache {
	return &Cache{template: t, places: Find(t, opts...)}
}

// NewString parses the given string and returns a Cache to remember where the positions are inside the template
func NewString(t string, opts ...Option) *Cache {
	return New([]byte(t), opts...)
}

// Replace replaces the positions at the given places by using the given Replacer.
func (t *Cache) Replace(wr io.Writer, replacer Replacer) {
	Replace(t.template, wr, t.places, replacer)
}

// positions returns the positions by running through the template
func (t *Cache) Positions() []string {
	return Positions(t.template, t.places)
}
