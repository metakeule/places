package places

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"sort"
	"strings"
	"testing"
)

var _template2 = []byte{}
var expected = ""
var _map = map[string]string{}
var _mapReader = map[string]io.ReadSeeker{}

func Prepare() {
	_map = map[string]string{}
	_mapReader = map[string]io.ReadSeeker{}
	orig2 := []string{}
	exp := []string{}
	for i := 0; i < 5; i++ {
		orig2 = append(orig2, fmt.Sprintf(`a string with [@replacement%v@]`, i))
		exp = append(exp, fmt.Sprintf("a string with repl%v", i))
		_map[fmt.Sprintf("replacement%v", i)] = fmt.Sprintf("repl%v", i)
		_mapReader[fmt.Sprintf("replacement%v", i)] = strings.NewReader(fmt.Sprintf("repl%v", i))
	}
	expected = strings.Join(exp, "")
	_template2 = []byte(strings.Join(orig2, ""))
}

func TestFindAndReplace(t *testing.T) {
	Prepare()
	var buffer bytes.Buffer
	if FindAndReplace(_template2, &buffer, ReadSeekerMap(_mapReader)); buffer.String() != expected {
		t.Errorf("unexpected result: %#v, expected: %#v", buffer.String(), expected)
	}
}

func TestPosition(t *testing.T) {
	Prepare()
	got := New(_template2).Positions()
	sort.Strings(got)
	expected := []string{"replacement0", "replacement1", "replacement2", "replacement3", "replacement4"}
	if !reflect.DeepEqual(got, expected) {
		t.Errorf("unexpected result: %#v, expected: %#v", got, expected)
	}
}

func TestAjacentPosition(t *testing.T) {
	var buffer bytes.Buffer
	tt := []byte("a string with [@replacement0@][@replacement1@] after")
	ph := Find(tt)
	exp := "a string with repl0repl1 after"

	if Replace(tt, &buffer, ph, ReadSeekerMap(_mapReader)); buffer.String() != exp {
		t.Errorf("unexpected result: %#v, expected: %#v", buffer.String(), exp)
	}
}

func TestReplaceString(t *testing.T) {
	Prepare()
	var buffer bytes.Buffer
	if Replace(_template2, &buffer, Find(_template2), StringMap(_map)); buffer.String() != expected {
		t.Errorf("unexpected result: %#v, expected: %#v", buffer.String(), expected)
	}
}
