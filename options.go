package places

var (
	// DefaultStartDel is the default start delimiter (prefix of the position)
	DefaultStartDel = []byte("[@")

	// DefaultEndDel is the default end delimiter (postfix of the position)
	DefaultEndDel = []byte("@]")
)

type options struct {
	startDel []byte
	endDel   []byte
}

func defaultOptions() *options {
	return &options{DefaultStartDel, DefaultEndDel}
}

// Option is a parsing option
type Option func(c *options)

// WithDelimiter sets custom delimiters of the positions
// The starting and delimiters must be 2 bytes long and must not be equal.
// We panic, if these rules are not fullfilled.
func WithDelimiter(start, end string) Option {
	if start == end {
		panic("start and end delimiters must not be equal")
	}

	if len(start) != 2 {
		panic("start delimiter must be two byte long")
	}

	if len(end) != 2 {
		panic("en delimiter must be two byte long")
	}

	return func(c *options) {
		c.startDel = []byte(start)
		c.endDel = []byte(end)
	}
}
